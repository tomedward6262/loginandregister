<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function  check(Request $request){
        $data=[
            'name'      => $request -> name,
            'email'     => $request -> email,
            'password'  => $request -> password,
        ];
        if(Auth::attempt($data)){
            return true;
        }else{
            return false;
        }
    }
}
