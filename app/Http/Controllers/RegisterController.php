<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\User;
use Validator,Input;


class RegisterController extends Controller
{
    public function construct()
    {
        $this -> middleware(['auth' => 'verified']);
    }

    function index(){
        return view('register');
    }

    public function storage(Request $request){
        $validateData = Validator::make($request->all(),[
            'name'      => ['required', 'string', 'max:255'],
            'email'     => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'  => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        if($validateData -> fails()){
            return Redirect::back()
            ->withErrors($validateData)
            ->withInput();
            //$input = input::all();
            return Redirect::to('/register')->withInput();
        }else{
            User::create([
                'name'      => $request->name,
                'email'     => $request->email,
                'password'  => password_hash($request->password, PASSWORD_BCRYPT),
                'remember_token' => mt_rand(50,60),
            ]);

            return Redirect::to('/register')->with('response', 'Dang ky thanh cong');
        }
    }

}


