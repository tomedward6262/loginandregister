<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Login Form</title>
</head>
<body>
<br/>
<div class="container">
    <div class="form-group col-md-12" align="right"><a href="http://127.0.0.1:8000/login"><button>Log in</button></a>  <a href="http://127.0.0.1:8000/register"><button>Register</button></a></div>
</div>
<div class="container">
    <h1 align="center" style="color: darkorange">Đăng nhập</h1><br/>

    @if(isset(Auth::user()->email))
        <script>window.location = "/login/successLogin";</script>
    @endif

    @if($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong>{{$message}}</strong>
        </div>
    @endif

    <div class = "row">
        <div class="col-md-4"></div>
        <div class= "col-md-4">
            <form method = "post" action = "{{url('/login/checkLogin')}}" id="loginForm">
                {{csrf_field()}}
                <div class = "form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="login">Email</label>
                    <input type="email" name = "email" value="" class = "form-control"/>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class = "form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label>Mật khẩu</label>
                    <input type="password" name = "password" value="" class  = "form-control" />
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <input type="submit" name="login" class="btn btn-primary" value="Đăng nhập"/>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
