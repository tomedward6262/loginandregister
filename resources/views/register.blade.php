<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Register Form</title>
</head>
<body>
<div class="container"><br>
    <div class="form-group col-md-12" align="right"><a href="http://127.0.0.1:8000/login"><button>Log in</button></a>  <a href="http://127.0.0.1:8000/register"><button>Register</button></a></div>
</div>
<div class="container ">
    <h1 align="center" style="color: lightseagreen">Đăng ký</h1><br/>
    @if(session('response'))
        <div class="alert alert-success">{{session('response')}}</div>
    @endif

    @if(session('fail'))
        <div class="alert alert-success">{{session('fail')}}</div>
    @endif
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-md-offset-2">

                                    <form  method="post" action="{{ route('registerPOST') }}">

                                        {!! csrf_field() !!}

                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label class="col-md-4 control-label">Name</label>

                                            <div class="col-md-12">
                                                <input align="right" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label class="col-md-4 control-label">Email</label>

                                            <div class="col-md-12">
                                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label class="col-md-4 control-label">Password</label>

                                            <div class="col-md-12">
                                                <input type="password" class="form-control" name="password" required>

                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                            <label class="col-md-12 control-label">Confirm Password</label>

                                            <div class="col-md-12">
                                                <input type="password" class="form-control" name="password_confirmation" required>

                                                @if ($errors->has('password_confirmation'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="checkbox" > Dong y voi cac <a href="">Dieu khoan va dich vu</a>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-11 col-md-offset-4" align="right">
                                                <button type="submit" name="register_success" class="btn btn-info">Đăng ký</button>
                                            </div>
                                        </div>
                                    </form>
                             <div>
                         <div>
                     <div>
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
